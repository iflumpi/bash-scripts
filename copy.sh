#! /usr/local/bin/bash

copy(){
	# This script was made to copy password from a file
	# into clipboard using xclip utility
	# -------------------------------------------------
	# @ example
	# $ cat natu
	#   username: natutu
	#   passwd: natutu2893486
	# 
	# $ copy natu
	#   password was successfully copied to clipboard
	# @ end-example
	# -------------------------------------------------
	# Then you can paste it whatever place
	
	if [ -z  $1 ]; then
		echo "usage: copy [filename]"
		exit
	fi
	passwd=$(cat $1 | egrep -o "^(passwd|password)[:]?[[:blank:]]?[a-zA-Z0-\9\#\$\@\!\%\&\(\)[:blank:]]*$" | \
			xargs | cut -d' ' -f2)
	if ! [ -z $passwd ]; then
		echo "$passwd" | xclip -selection clipboard
		echo "password was successfully copied to clipboard [password length: ${#passwd}]"
	else
		echo "error copying the password to clipboard"
	fi
}
export copy
