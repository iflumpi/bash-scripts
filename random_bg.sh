#!/usr/local/bin/bash

# Random background image.

# This function uses feh with X11 to set the wallpaper with a random or randoms
# images when are available two screens.
# It takes the images by default from `${USERDIR}/wallpapers/wallpapers` directory
# but you can change it with --wdir or -w flag before the name of the sugested
# directory.

# This file is under the GPLv3 license, you can download, modify and distribute it
# under the license terms.

# Author: Yassin Achengli tao3000ac@protonmail.com

random_background(){
    local directory_switched=0
    local diferent_wallpapers=0
    local background_directory=${HOME}/wallpapers/wallpapers
    for opt in $@; do
	case $opt in
	    "--diff"|"-d")
		diferent_wallpapers=1;;
	    "--help"|"-h")
		display_help
		return;;
	    "--wdir"|"-w")
		directory_switched=1;;
	    *)
		if [[ $directory_switched == 1 ]]; then
		    background_directory=$opt
	        else
		    echo "$opt is not a valid option"
		    return
		fi
		directory_switched=0;;
	esac
    done
    IFS_P=$IFS
    IFS=" "
    declare -a all_images
    local all_images=$background_directory/*
    all_images=($all_images)
    local all_images_length=${#all_images[@]}
    local random_image=${all_images[$(( ($RANDOM % ${all_images_length})+1 ))]}
    if [[ $diferent_wallpapers == 1 ]]; then
	random_image_b=${all_images[$((($RANDOM % ${all_images_length})+1))]}
	feh --bg-scale ${random_image} --bg-fill ${random_image_b};
    else
	feh --bg-scale ${random_image} --bg-fill ${random_image};
    fi
    IFS=$IFS_P
}

display_help(){
    echo -e "Change the background of your desktop with maximum two monitors.\n\
Options:\n\
---------\n\
--diff | -d : Puts two diferent wallpaper images one for each screen. (uses xrandr)\n\
--help | -h : Shows this message\n\
--wdir | -w : Switch to other directory, the directory path must be after this option\n\
              with blank space between"
}
