#!/bin/bash

# Check if first block (512 bytes) of /dev/sda device contains the MBR magic number in last word (0x55aa)

dd if=/dev/sda of=sda.bin count=1  
last_word=$(xxd sda.bin | tail -n 1 | cut -d" " -f9)
if [ $last_word == "55aa" ]; then
    echo "First block of /dev/sda contains MBR magic number"
else
    echo "First block of /dev/sda does NOT contain MBR magic number"
fi
