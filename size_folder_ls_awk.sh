#!/bin/bash

# Calculate size of a folder (bytes) by using ls and awk

/bin/ls -lR $1 | awk 'BEGIN { size = 0 }; { if (NF == 9) { size += $5 } }; END { printf("%d\n", size) }'
