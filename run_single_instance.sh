#! /bin/env bash
# Running script as single instance, if it's on ram another process
# with the same name, this script will kill it and then
# execute the script passed as argument

if [ -n $1 ]; then
	program_name="$1"
	ps_result=$(ps aux \
		| grep "${program_name}" \
		| grep -v "grep" \
		| wc -l 
		 )
	
	if [ ${ps_result} -gt 0 ];then
	    echo "$program_name already running"
	    echo "closing $program_name before run it again"
	    # I think using pkill maybe the problem will be solved
	    # Anyways I will use kill
	    declare -i program_process
	    # This sentence extracts only the process name avoiding the
	    # grep process and making trim over the response before extract
	    # the PID number
	    program_process=$(ps aux | grep $program_name | grep -v "grep" \
				   | xargs | cut -d' ' -f2)
	    result=$(kill -9 $program_process)
	    if [[ ${result} -gt -1 ]];then
	       echo -e "\e[0;31mprocess $program_process was killed, you're a killer!\e[m"
	    fi
	fi

	echo "Running $program_name"
	$@ &
else
	echo "nothing to do"
fi
