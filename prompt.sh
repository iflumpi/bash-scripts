#!/usr/local/bin/bash

# Prompt function to get informed of the status of the current
# repository if you're inside a directory where .git coexists.

git_response() {
    P="\[\e[38;1;2;1;23m\]\u "
    if [[ $(git status -s) ]]; then
	local status=($(git status -s))
	if [[ ${status[0]} == "??" ]]; then
	    P="${P}\[\e[m\]\[\e[38;2;233;34;30m\]\[\e[1m\]\
b:$(git branch --show-current) [ ?? ]"
	else
	    P="${P}\[\e[m\]\[\e[38;2;123;124;240m\]\[\e[1m\]\
b:$(git branch --show-current) [ AA ]"
	fi
    else
	P="${P}\[\e[33;2;122;32;23m\]b:$(git branch --show-current)" 
    fi
    P="${P}\[\e[0m\]\[\e[38;1;2;1;23m\] λ \[\e[0m\]"
    echo  "$P"
}

prompt () {
    if [ -d .git ]; then
	PS1="$(git_response)"
    else
	PS1="\[\e[38;2;245;245;245m\]\[\e[1m\]\u λ \[\e[0m\]"
    fi	
}

PROMPT_COMMAND=prompt
export PROMPT_COMMAND
