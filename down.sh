#!/usr/local/bin/bash

# file: 
# pornpics.com image downloader

if [ -f tempo.html ]
then
    rm tempo.html
fi

wget $1 --output-document tempo.html
content=$(cat tempo.html)

img_urls=$(echo $content | grep -o -E "data-src[\=][\'|\"](.+[\.](png|jpg))[\'|\"]")
dir="/home/tao/.pics/imagenes-$( date +'%d-%m-%y' )"
mkdir -p $dir


for img in $img_urls
do
    if [[ ${#img} -gt 10 ]]
    then
	if [[ "${img:0:8}" == "data-src" ]]
	then
	    img_src="${img:10:$(( ${#img} - 11 ))}"
	    IFS_P=$IFS
	    IFS="/"
	    read -ra img_name <<< "${img_src}"
	    img_name=${img_name[$(( ${#img_name} + 1 ))]}
	    IFS=$IFS_P
	    echo -e "\033[0;32mDownloading:\033[m ${img_src} to $img_name"
	    wget "${img_src}" --output-document "${dir}/${img_name}"

	fi
    fi
done
